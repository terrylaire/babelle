# BABELLE

This is a copy of Worldle that presents a city. You can play here: https://babelle.terrylaire.fr  
Here is the original Worldle : https://worldle.teuteuf.fr

## Contributions & PR

I do not plan to accept PRs nor suggestions, sorry!

Feel free to fork the project, customize it and play with it on your side! <3

## Resources used:

- Worldle => https://github.com/teuteuf/worldle
- Cities with countries => https://public.opendatasoft.com/explore/dataset/geonames-all-cities-with-a-population-1000/table/ (CC by https://www.geonames.org/about.html)
- Countries with long/lat => https://developers.google.com/public-data/docs/canonical/countries_csv
- Country area => https://github.com/samayo/country-json/blob/master/src/country-by-surface-area.json
- French country names => https://fr.wikipedia.org/wiki/ISO_3166
- Country images => https://github.com/djaiss/mapsicon
- Fixed images => http://www.amcharts.com/svg-maps/ & Wikipedia
- Emojis & World icon => https://github.com/jdecked/twemoji/
