import React from "react";

export function Worldle() {
  return (
    <span className="font-bold">
      BABEL<span className="text-orange-800">LE</span>
    </span>
  );
}
